import {BaseExceptionFilter} from "@nestjs/core";
import {ArgumentsHost, Catch, NotFoundException} from "@nestjs/common";
import {Request, Response} from 'express';

@Catch(NotFoundException)
export class HttpExceptionFilter extends BaseExceptionFilter {
    catch(exception: any, host: ArgumentsHost) {
        const context = host.switchToHttp();
        const response = context.getResponse<Response>();
        const request = context.getRequest<Request>();
        const status = exception.status;
        const message = exception.message;

        response
            .status(status)
            .json({
                message,
                statusCode: status,
                time: new Date().toISOString()
            });
    }
}
