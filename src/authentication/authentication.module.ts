import {Module} from "@nestjs/common";
import {UserModule} from "../user/user.module";
import {AuthenticationService} from "./authentication.service";
import {ConfigModule, ConfigService} from "@nestjs/config";
import {JwtModule} from "@nestjs/jwt";
import {AuthenticationController} from "./authentication.controller";
import {PassportModule} from "@nestjs/passport";
import {LocalStrategy} from "./local.strategy";
import {JwtStrategy} from "./jwt.strategy";
import {UserVerificationModule} from "../user-verification/user-verification.module";
import {MailingIntegrationModule} from "../mailing-integration/mailing-integration.module";

@Module({
    imports: [
        UserModule,
        UserVerificationModule,
        MailingIntegrationModule,

        PassportModule,
        ConfigModule,
        JwtModule.registerAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: async (configService: ConfigService) => ({
                secret: configService.get('JWT_SECRET'),
                signOptions: {
                    expiresIn: `${configService.get('JWT_EXPIRATION_TIME')}s`,
                },
            })
        })
    ],
    providers: [AuthenticationService, LocalStrategy, JwtStrategy],
    controllers: [AuthenticationController]
})
export class AuthenticationModule {

}
