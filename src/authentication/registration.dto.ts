import {IsEmail, IsNotEmpty, IsString} from "class-validator";

export class RegistrationDto {
    @IsString()
    @IsNotEmpty()
    name: string;

    @IsString()
    @IsNotEmpty()
    login: string;

    @IsString()
    @IsNotEmpty()
    password: string;

    @IsEmail()
    email: string;
}
