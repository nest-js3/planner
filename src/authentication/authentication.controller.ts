import {Body, Controller, Get, HttpCode, Post, Req, UseGuards} from "@nestjs/common";
import RequestWithUser from "./requestWithUser.interface";
import {AuthenticationService} from "./authentication.service";
import {RegistrationDto} from "./registration.dto";
import {LocalAuthenticationGuard} from "./localAuthentication.guard";
import JwtAuthenticationGuard from "./jwtAuthentication.guard";

@Controller('authentication')
export class AuthenticationController {
    constructor(
        private readonly authenticationService: AuthenticationService
    ) {
    }


    @Post('register')
    async register(@Body() registrationData: RegistrationDto) {
        return this.authenticationService.register(registrationData);
    }


    @HttpCode(200)
    @UseGuards(LocalAuthenticationGuard)
    @Post('login')
    async logIn(@Req() request: RequestWithUser) {
        const {user} = request;
        const cookie = this.authenticationService.getCookieWithJwtToken(user.id);
        request.res.setHeader('Set-Cookie', cookie);
        user.password = undefined;
        return request.res.send(user);
    }

    @UseGuards(JwtAuthenticationGuard)
    @Get()
    authenticate(@Req() request: RequestWithUser) {
        const user = request.user;
        user.password = undefined;
        return user;
    }

    @UseGuards(JwtAuthenticationGuard)
    @Post('logout')
    async logOut(@Req() request: RequestWithUser) {
        request.res.setHeader('Set-Cookie', this.authenticationService.getCookieForLogOut());
        return request.res.sendStatus(200);
    }
}
