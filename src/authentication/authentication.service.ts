import {UserService} from "../user/user.service";
import {HttpException, HttpStatus, Injectable} from "@nestjs/common";
import * as bcrypt from 'bcrypt';
import {RegistrationDto} from "./registration.dto";
import PostgresErrorCode from "../database/postgresErrorCode.enum";
import {JwtService} from "@nestjs/jwt";
import {ConfigService} from "@nestjs/config";
import {MailingIntegrationService} from "../mailing-integration/mailing-integration.service";
import {v4 as uuidV4} from 'uuid';
import {UserVerificationService} from "../user-verification/user-verification.service";
import {Connection} from "typeorm";

@Injectable()
export class AuthenticationService {
    constructor(
        private readonly userService: UserService,
        private readonly jwtService: JwtService,
        private readonly configService: ConfigService,
        private readonly verificationService: UserVerificationService,
        private readonly mailingService: MailingIntegrationService,
        private connection: Connection
    ) {
    }

    public async register(registrationData: RegistrationDto) {
        const hashedPassword = await bcrypt.hash(registrationData.password, 10);
        const queryRunner = this.connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            const createdUser = await this.userService.createUserTransactionaly({
                ...registrationData,
                password: hashedPassword
            }, queryRunner);
            const verificationToken = uuidV4();

            await this.verificationService.createVerificationTransactionaly(verificationToken, createdUser, queryRunner);
            this.mailingService.sendMail({
                login: createdUser.login,
                email: createdUser.email,
                name: createdUser.name
            }, verificationToken);

            await queryRunner.commitTransaction();
            createdUser.password = undefined;
            return createdUser;
        } catch (error) {
            console.log(error)
            await queryRunner.rollbackTransaction();
            if (error?.code === PostgresErrorCode.UniqueViolation) {
                throw new HttpException('User with that email already exists', HttpStatus.BAD_REQUEST);
            }
            throw new HttpException('Something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {
            await queryRunner.release();
        }
    }

    public async getAuthenticatedUser(login: string, hashedPassword: string) {
        try {
            const user = await this.userService.getUserByLogin(login);
            const isPasswordMatching = await bcrypt.compare(
                hashedPassword,
                user.password
            );
            if (!isPasswordMatching) {
                throw new HttpException('Wrong credentials provided', HttpStatus.BAD_REQUEST);
            }
            user.password = undefined;
            return user;
        } catch (error) {
            throw new HttpException('Wrong credentials provided', HttpStatus.BAD_REQUEST);
        }
    }

    public getCookieWithJwtToken(userId: number) {
        const payload: TokenPayload = {userId};
        const token = this.jwtService.sign(payload);
        return `Authentication=${token}; HttpOnly; Path=/; Max-Age=${this.configService.get('JWT_EXPIRATION_TIME')}`;
    }

    public getCookieForLogOut() {
        return `Authentication=; HttpOnly; Path=/; Max-Age=0`;
    }
}
