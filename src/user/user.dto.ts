export class UserDto {
    name: string;
    email: string;
    login: string;
    password: string;
}

export class UserUpdateDto {
    name: string;
    login: string;
    password: string;
}
