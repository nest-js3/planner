import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {UserDto, UserUpdateDto} from "./user.dto";
import {User} from "./user.entity";
import {InjectRepository} from "@nestjs/typeorm";
import {Equal, QueryRunner, Repository} from "typeorm";

@Injectable()
export class UserService {

    constructor(@InjectRepository(User)
                private usersRepository: Repository<User>) {

    }

    public async createUser(userDto: UserDto) {
        const newUser = await this.usersRepository.create(userDto);
        await this.usersRepository.save(newUser);
        return newUser;
    }

    public async createUserTransactionaly(userDto: UserDto, queryRunner: QueryRunner) {
        const newUser = await this.usersRepository.create(userDto);
        await queryRunner.manager.save(newUser);
        return newUser;
    }

    public async getUserByLogin(login: string) {
        const user = await this.usersRepository.findOne({
            where: {login: Equal(login)}
        })
        if (user) {
            return user;
        }
        throw new HttpException('User couldn\'t be found', HttpStatus.NOT_FOUND);
    }

    public async updateUser(id: number, userUpdateDto: UserUpdateDto) {
        await this.usersRepository.update(id, userUpdateDto);
        const updatedUser = await this.usersRepository.findOne(id);
        if (updatedUser) {
            return updatedUser
        }
        throw new HttpException('Post not found', HttpStatus.NOT_FOUND);
    }

    // public async deleteUser(id: number) {
    //     const deleteResult = await this.usersRepository.delete(id);
    //     if (!deleteResult.affected) {
    //         throw new HttpException('User couldn\'t be found', HttpStatus.NOT_FOUND);
    //     }
    // }

    public async getUser(userId: number) {
        const user = await this.usersRepository.findOne(userId)
        if (user) {
            return user;
        }
        throw new HttpException('User couldn\'t be found', HttpStatus.NOT_FOUND);
    }


    // public async getUsers() {
    //     const users = await this.usersRepository.find();
    //     return users.map(user => {
    //         return this.mapToDto(user);
    //     })
    // }

    //
    // private mapToDto(user: User): MailingDataDto {
    //     return {
    //         name: user.name,
    //         login: user.login
    //     };
    // }
}
