import {Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";


export enum UserState {
    PENDING = 'P',
    ACTIVE = 'A'
}

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;
    @Column({nullable: false})
    name: string;
    @Column({unique: true, nullable: false})
    login: string;
    @Column({unique: true, nullable: false})
    email: string
    @Column({nullable: false})
    password: string;

    @Column({
        type: "enum",
        enum: UserState,
        default: UserState.PENDING
    })
    userState: UserState;

}

