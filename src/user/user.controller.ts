import {Body, Controller, Delete, Get, Param, Post, Put} from '@nestjs/common';
import {UserService} from './user.service';
import {UserDto, UserUpdateDto} from "./user.dto";

@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService) {
    }
    //
    // @Post()
    // createUser(@Body() userDto: MailingDataDto) {
    //     return this.userService.createUser(userDto);
    // }
    //
    // @Get(':id')
    // getUser(@Param('id') userId: number) {
    //     return this.userService.getUser(userId);
    // }
    //
    //
    // @Get()
    // getUsers() {
    //     return this.userService.getUsers();
    // }
    //
    // @Put(':id')
    // updateUser(@Param('id') userId: number, @Body() userUpdateDto: UserUpdateDto) {
    //     return this.userService.updateUser(userId, userUpdateDto);
    // }
    //
    //
    // @Delete(':id')
    // deleteUser(@Param('id') userId: number) {
    //     return this.userService.deleteUser(userId);
    // }
}
