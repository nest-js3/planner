import {HttpException, HttpStatus, Injectable, UseGuards} from '@nestjs/common';
import {TaskDto, TaskUpdateDto} from "./task.dto";
import {Task} from "./task.entity";
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";

@Injectable()
export class TaskService {

    constructor(@InjectRepository(Task)
                private tasksRepository: Repository<Task>) {

    }

    public async createTask(taskDto: TaskDto) {
        const newTask = await this.tasksRepository.create(taskDto);
        await this.tasksRepository.save(newTask);
        return newTask;
    }

    public async updateTask(id: number, taskUpdateDto: TaskUpdateDto) {
        await this.tasksRepository.update(id, taskUpdateDto);
        const updatedTask = await this.tasksRepository.findOne(id);
        if (updatedTask) {
            return updatedTask
        }
        throw new HttpException('Post not found', HttpStatus.NOT_FOUND);
    }

    public async deleteTask(id: number) {
        const deleteResult = await this.tasksRepository.delete(id);
        if (!deleteResult.affected) {
            throw new HttpException('Task couldn\'t be found', HttpStatus.NOT_FOUND);
        }
    }

    public async getTask(taskId: number) {
        const task = await this.tasksRepository.findOne(taskId)
        if (task) {
            return this.mapToDto(task);
        }
        throw new HttpException('Task couldn\'t be found', HttpStatus.NOT_FOUND);
    }

    public async getTasks() {
        const tasks = await this.tasksRepository.find();
        return tasks.map(t => {
            return this.mapToDto(t);
        })
    }

    // private map(taskDto: TaskDto): Task {
    //     return {
    //         id: null,
    //         header: taskDto.header,
    //         content: taskDto.content,
    //         priority: taskDto.priority,
    //         user_id: taskDto.user_id,
    //     };
    // }
    //
    // private mapForUpdate(taskDto: TaskUpdateDto): Task {
    //     return {
    //         id: null,
    //         header: taskDto.header,
    //         content: taskDto.content,
    //         priority: taskDto.priority,
    //         user_id: taskDto.user_id,
    //     };
    // }

    private mapToDto(task: Task): TaskDto {
        return {
            header: task.header,
            content: task.content,
            priority: task.priority,
            userId: task.user.id,
        }
    }
}
