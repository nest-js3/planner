import {Body, Controller, Delete, Get, Param, Patch, Post, UseGuards} from '@nestjs/common';
import {TaskService} from './task.service';
import {TaskDto, TaskUpdateDto} from "./task.dto";
import JwtAuthenticationGuard from "../authentication/jwtAuthentication.guard";

@Controller('task')
@UseGuards(JwtAuthenticationGuard)
export class TaskController {
    constructor(private readonly taskService: TaskService) {
    }

    @Post()
    createTask(@Body() taskDto: TaskDto) {
        return this.taskService.createTask(taskDto);
    }

    @Get(':id')
    getTask(@Param('id') taskId: number) {
        return this.taskService.getTask(taskId);
    }

    @Get()
    getTasks() {
        return this.taskService.getTasks();
    }

    @Patch(':id')
    updateTask(@Param('id') taskId: number, @Body() taskUpdateDto: TaskUpdateDto) {
        return this.taskService.updateTask(taskId, taskUpdateDto);
    }

    @Delete(':id')
    deleteTask(@Param('id') taskId: number) {
        return this.taskService.deleteTask(taskId);
    }
}
