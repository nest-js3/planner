import {Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, RelationId} from "typeorm";
import {User} from "../user/user.entity";

@Entity()
export class Task {
    @PrimaryGeneratedColumn()
    public id: number;
    @Column({unique: true})
    public header: string;
    @Column()
    public content: string;
    @Column()
    public priority: string;

    @ManyToOne(() => User, {nullable: false})
    @JoinColumn()
    public user: User;
}
