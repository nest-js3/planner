import {NotFoundException} from "@nestjs/common";

export class TaskNotFoundException extends NotFoundException {
    constructor(taskId: number) {
        super(`Can't find task with id ${taskId}`);
    }
}
