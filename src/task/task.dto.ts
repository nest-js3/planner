import {IsNotEmpty, IsString} from "class-validator";

export class TaskDto {
    @IsString()
    @IsNotEmpty()
    header: string;
    @IsString()
    @IsNotEmpty()
    content: string;
    @IsString()
    @IsNotEmpty()
    priority: string;
    userId: number;
}

export class TaskUpdateDto {
    header: string;
    content: string;
    priority: string;
    userId: number;
}
