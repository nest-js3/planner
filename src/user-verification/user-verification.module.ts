import {Module} from '@nestjs/common';
import {UserVerificationController} from './user-verification.controller';
import {UserVerificationService} from './user-verification.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {UserVerification} from "./userVerification.entity";

@Module({
    imports: [TypeOrmModule.forFeature([UserVerification])],
    controllers: [UserVerificationController],
    providers: [UserVerificationService],
    exports: [UserVerificationService]
})
export class UserVerificationModule {
}
