import {Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, RelationId} from "typeorm";
import {User} from "../user/user.entity";

export enum TokenState {
    NEW,
    VERIFIED
}

@Entity()
export class UserVerification {
    @PrimaryGeneratedColumn()
    id: number;

    @JoinColumn()
    @ManyToOne(() => User, {nullable: false})
    user: User;

    @RelationId((verification: UserVerification) => verification.user)
    userId: number;

    @Column({nullable: false, unique: true})
    token: string;

    @Column({
        type: "enum",
        enum: TokenState,
        default: TokenState.NEW
    })
    tokenState: TokenState;
}
