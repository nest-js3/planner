import {Controller, Get, Param} from '@nestjs/common';
import {UserVerificationService} from "./user-verification.service";
import {TokenState} from "./userVerification.entity";

@Controller('verification')
export class UserVerificationController {
    constructor(
        private verificationService: UserVerificationService
    ) {
    }

    @Get(':token')
    public verifyUser(@Param('token') token: string) {
        return this.verificationService.updateVerificationState(token, TokenState.VERIFIED);
    }
}
