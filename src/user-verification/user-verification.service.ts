import {Injectable, NotFoundException} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {TokenState, UserVerification} from "./userVerification.entity";
import {QueryRunner, Repository} from "typeorm";
import {User} from "../user/user.entity";

@Injectable()
export class UserVerificationService {

    constructor(
        @InjectRepository(UserVerification)
        private readonly verificationRepository: Repository<UserVerification>
    ) {
    }

    public async createVerification(token: string, user: User) {
        const userVerification = this.verificationRepository.create({
            user: user,
            token: token
        });
        await this.verificationRepository.save(userVerification);
        return userVerification;
    }

    public async createVerificationTransactionaly(token: string, user: User, queryRunner: QueryRunner) {
        const userVerification = this.verificationRepository.create({
            user: user,
            token: token
        });
        await queryRunner.manager.save(userVerification);
        return userVerification;
    }


    public async updateVerificationState(token: string, state: TokenState) {
        let updateResult = await this.verificationRepository.update({token: token}, {tokenState: state});
        if (!updateResult.affected) {
            throw new NotFoundException(`User verification couldn't be found by token: ${token}`);
        }
    }

}
