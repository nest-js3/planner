import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {UserModule} from "./user/user.module";
import {TaskModule} from "./task/task.module";
import {ConfigModule} from '@nestjs/config';
import * as Joi from '@hapi/joi';
import {DatabaseModule} from "./database/database.module";
import {AuthenticationModule} from './authentication/authentication.module';
import {APP_FILTER} from "@nestjs/core";
import {HttpExceptionFilter} from "./utils/httpExceptionFilter";
import {UserVerificationModule} from "./user-verification/user-verification.module";
import {MailingIntegrationModule} from "./mailing-integration/mailing-integration.module";

@Module({
    imports: [
        TaskModule,
        UserModule,
        UserVerificationModule,
        MailingIntegrationModule,
        AuthenticationModule,

        ConfigModule.forRoot({
            validationSchema: Joi.object({
                POSTGRES_HOST: Joi.string().required(),
                POSTGRES_PORT: Joi.number().required(),
                POSTGRES_USER: Joi.string().required(),
                POSTGRES_PASSWORD: Joi.string().required(),
                POSTGRES_DB: Joi.string().required(),
                PORT: Joi.number(),
                JWT_SECRET: Joi.string().required(),
                JWT_EXPIRATION_TIME: Joi.string().required(),
            })
        }),
        DatabaseModule,
    ],
    controllers: [AppController],
    providers: [AppService, {
        provide: APP_FILTER,
        useClass: HttpExceptionFilter
    }],
})
export class AppModule {
}
