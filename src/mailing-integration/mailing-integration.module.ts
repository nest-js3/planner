import {Module} from "@nestjs/common";
import {ConfigModule, ConfigService} from "@nestjs/config";
import {ClientProxyFactory, Transport} from "@nestjs/microservices";
import {MailingIntegrationService} from "./mailing-integration.service";

@Module({
    imports: [ConfigModule],
    providers: [
        MailingIntegrationService,
        {
            provide: 'MAILING_INTEGRATION',
            useFactory: (configService: ConfigService) => {
                const user = configService.get('RABBITMQ_USER');
                const password = configService.get('RABBITMQ_PASSWORD');
                const host = configService.get('RABBITMQ_HOST');
                const queueName = configService.get('RABBITMQ_QUEUE_NAME');

                return ClientProxyFactory.create({
                    transport: Transport.RMQ,
                    options: {
                        urls: [`amqp://${user}:${password}@${host}`],
                        queue: queueName,
                        queueOptions: {
                            durable: false,
                        },
                    },
                })
            },
            inject: [ConfigService],
        }
    ],
    exports: [MailingIntegrationService]
})
export class MailingIntegrationModule {

}
