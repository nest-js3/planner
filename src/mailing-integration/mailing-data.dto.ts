export class MailingDataDto {
    name: string;
    login: string;
    email: string;
}
