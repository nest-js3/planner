import {Inject, Injectable} from "@nestjs/common";
import {ClientProxy} from "@nestjs/microservices";
import {MailingDataDto} from "./mailing-data.dto";

@Injectable()
export class MailingIntegrationService {
    constructor(
        @Inject('MAILING_INTEGRATION') private mailingClient: ClientProxy,
    ) {
    }

    public sendMail(dataDto: MailingDataDto, verificationToken: string) {
        console.log('Sending data to: ' + verificationToken);
        return this.mailingClient.send({
            cmd: 'sendMail'
        }, {
            ...dataDto,
            token: verificationToken
        }).subscribe(value => {
            console.log("Received value.")
            console.log(value);
        }, err => {
            console.log("error occured");
            console.log(err);
        })
    }
}
